"""
pydxp

A python wrapper around the Nectar DXP dashboard REST API.
"""
from .pydxp import *
