# pydxp: A Python wrapper for Nectar DXP
pydxp is a python wrapper around the Nectar DXP dashboard REST API. 

# Installation
`pip install pydxp`

# Getting started
The first thing to do to get started is import module and authenticate with your credentials.

```python
from pydxp import NectarAuth

login = NectarAuth(
    domain="https://demo.us.nectar.services", tenant="regex", authusr="demo1@nectarcorp.com",
    authpwd="PASSWORD"
)

print(login.retrieve_credentials())
```

| Parameter | Description                                                                                                               |
|---------|---------------------------------------------------------------------------------------------------------------------------|
| domain  | The Nectar DXP domain you would like to connect to (defaults to https://us.nectar.services if not specified). *Optional*. |
| tenant  | Tenant name (use it for user’s that have access to multiple tenants). *Optional*.                                         |
| authusr | Username of the account which has got access to Nectar DXP.                                                               |
| authpwd | Corresponding password for the username being used.                                                                       |

## List of Available Endpoints
<details>
<summary>User Endpoints</summary>
<p>

## Search users by query string
```python
import json
from pydxp import NectarAuth
from pydxp import UserEndpoints

login = NectarAuth(
    domain="https://demo.us.nectar.services", tenant="regex", authusr="demo1@nectarcorp.com",
    authpwd="PASSWORD"
)

data = UserEndpoints.search_users_by_query_string(login, username="jane.doe@regex.com")
print(json.dumps(data))
```
| Parameter  | Description                                                                                                                           |
|------------|---------------------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                                 |
| pagenumber | Page number for paged response (starts from 1, default = 1). *Optional*. Example: pagenumber=1                                        |
| pagesize   | Page size for paged response (starts from 1, default = 10). *Optional*. Example: pagesize=10                                          |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

### Output
```json
{"totalElements": 1, "elements": [{"id": "837", "userId": "837_1_10_*", "photo": null, "userName": "jane.doe", "firstName": "Jane", "lastName": "Doe", "email": "jane.doe@regex.com", "phone": "123-456-7890", "location": null, "address": null, "uri": null, "deleted": false, "city": null, "postalCode": null, "country": null, "postOfficeBox": null, "pbxName": null, "userPbxNumber": null, "displayName": "Jane Doe", "sipAddress": null, "lineUri": null, "platform": "AD", "platformUserName": null, "platforms": ["TEAMS", "ENDPOINT_CLIENT"], "ispName": null, "locationType": null, "hasAdId": true, "hasPlatformId": null, "lastSessionDate": null, "sessionCount": null, "sessionGood": null, "sessionPoor": null, "sessionUnknown": null}]}
```
## Get user quantity summary
```python
UserEndpoints.get_user_quantity_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user quality summary
```python
UserEndpoints.get_user_quality_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user server summary
```python
UserEndpoints.get_user_server_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user client types summary
```python
UserEndpoints.get_user_client_types_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user devices summary
```python
UserEndpoints.get_user_devices_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user network types summary
```python
UserEndpoints.get_user_network_types_summary(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| timeperiod | Time period for data fetching (LAST_HOUR, LAST_DAY, LAST_WEEK, LAST_MONTH, LAST_QUARTER, LAST_YEAR, CUSTOM) *Optional*. |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user info
```python
UserEndpoints.get_user_info(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get advanced user info
```python
UserEndpoints.get_advanced_user_info(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get user advanced info only status
```python
UserEndpoints.get_user_advanced_info_only_status(login, username="jane.doe@regex.com", timeperiod="LAST_DAY")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| username   | Username to be queried on Nectar DXP.                                                                                   |
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

## Get pinned users
```python
UserEndpoints.get_pinned_users(login,tenant="regex")
```
| Parameter  | Description                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------|
| tenant     | Tenant name (use it for user’s that have access to multiple tenants. Defaults to tenant name used during authentication). *Optional*. |

</p>
</details>
